from django.db import models

def upload_dir(instance, filename):
    return '/'.join(['images', instance.username, filename])

class CustomUser(models.Model):
    email = models.EmailField()
    username = models.CharField(max_length=32, blank=False, null=False)
    date = models.DateTimeField(auto_now_add=True)
    verified = models.BooleanField()
    random = models.CharField(max_length=32)

class RequestedItem(models.Model):
    username = models.CharField(max_length=32, blank=False, null=False)
    date = models.DateTimeField(auto_now_add=True)
    iid = models.CharField(max_length=64)

class Item(models.Model):
    username = models.CharField(max_length=32, blank=False, null=False)
    iid = models.CharField(max_length=64)
    picture = models.FileField(upload_to=upload_dir, blank=False, null=False)
    thumbnail = models.CharField(max_length=100)
    active = models.BooleanField()
    upload_time = models.DateTimeField(auto_now_add=True)
    modify_time = models.DateTimeField(auto_now=True)
    description = models.TextField()
    numclicks = models.IntegerField()
    tags = models.TextField()

class PTrade(models.Model):
    owner = models.CharField(max_length=32, blank=False, null=False)
    requester = models.CharField(max_length=32, blank=False, null=False)
    item1 = models.CharField(max_length=100)
    item2 = models.CharField(max_length=100)
    seen = models.BooleanField()
    date = models.DateTimeField(auto_now_add=True)

class STrade(models.Model):
    owner = models.CharField(max_length=32)
    requester = models.CharField(max_length=32)
    item1 = models.CharField(max_length=100)
    item2 = models.CharField(max_length=100)
    date = models.DateTimeField(auto_now_add=True)
