View = {
	init : function() {
		View.init_render();
	},
	init_render : function() {
        $('#add-item').on({
        	'click' : function() {
        		console.log('female dogs');
        		$('#add-item-modal').modal('show');
        	},
        	'mouseover' : function() {
        		$(this).css('cursor', 'pointer');
        	}

        });
 
        $('.input-trade-is-requested').parent().tooltip({
            'selector' : '',
            'placement' : 'right',
            'title' : 'The owner of this item has been notified of your request'
        });

        $('.input-trade-request-nologin').parent().tooltip({
            'selector' : '',
            'placement' : 'right',
            'title' : 'You must be logged in to make a trade request.'
        });  
        
        $('.banner-header').on({
            'click' : function() {
                window.location = "/trade/main";
            },
            'mouseover' : function() {
                $(this).css('cursor', 'pointer');
            }

        });
        $('.input-trade-request').tooltip({
            'selector' : '',
            'placement' : 'right',
            'title' : 'Click to request a trade for this item!'
        });  

       

         $('#add-item').tooltip({
                'selector' : '',
                'placement' : 'right',
                'title' : 'Add another item to your inventory'
        });

         $('#items').hide().slideDown('slow');
         $('#upload-picture-header').tooltip({
                'selector' : '',
                'placement' : 'left',
                'title' : 'You have to add a picture for your item in order to add it to your inventory'
        });


        
         $('#add-new-item-btn').click(function() {
         	$('#add-item-modal').modal('hide');
         	data = {
         		description: $.trim($('#item-description-input').val()),
         		tags: $.trim($('#item-tags-input').val()),
         		itemname: $.trim($('#item-name-input').val())
         	};
         	console.log(document.getElementById("item-add-pic-input"));
         	$.post("/trade/add_item/", data, function(result){
    			console.log("returned that female dog");
  			});

           
         });

        $('#request-trade-btn').click(function() {
            $(this).addClass('disabled').text("Trade Requested");
        });

		 $('#login-btn').on({
        	'click' : function() {
        		$('#login-modal').modal('show');
                console.log('fsdf');
        	}

        });
        

         $('#submit-btn').click(function() {
         	$('#login-modal').modal('hide');
         	var data = {
         		username: $.trim($('#username-input').val()),
         		password: $.trim($('#password-input').val())
         	};
         	$.post("/trade/login/", data, function(result){
  			});
         });       
		
        $('#new-acc-box').hide();
        $('#new-account-btn').click(function() {
            $('#login-box').slideUp();
            $('#new-acc-box').slideDown();

        });

        $('#relogin-btn').click(function() {
            $('#login-box').slideDown();
            $('#new-acc-box').slideUp();
        });


        $('#new-password-input-verify').on('input', function() {

            console.log("hi");
            var pass = $.trim($('#new-password-input').val());
            var pass2 = $.trim($('#new-password-input-verify').val());
            if (pass != pass2) {
                $('#verify-password-msg').empty().append("The password does not match up.");
                $('#make-new-acc-btn').addClass('disabled');
            } else {
                $('#verify-password-msg').empty();
                $('#make-new-acc-btn').removeClass('disabled');
            }
        });

	}
}

$(document).ready(function() {

	View.init();
                
        
})
