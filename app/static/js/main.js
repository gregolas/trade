View = {
	init : function() {
		View.init_render();
	},
	init_render : function() {
        $('#login-btn').on({
        	'click' : function() {
        		$('#login-modal').modal('show');
                console.log('fsdf');
        	}

        });
        

         $('#submit-btn').click(function() {
         	$('#login-modal').modal('hide');
         	var data = {
         		username: $.trim($('#username-input').val()),
         		password: $.trim($('#password-input').val())
         	};
         	$.post("/trade/login/", data, function(result){
  			});
         });       

        $('.input-trade-is-requested').parent().tooltip({
            'selector' : '',
            'placement' : 'right',
            'title' : 'The owner of this item has been notified of your request'
        });

        $('.input-trade-request-nologin').parent().tooltip({
            'selector' : '',
            'placement' : 'right',
            'title' : 'You must be logged in to make a trade request.'
        });  

        $('.input-trade-request').tooltip({
            'selector' : '',
            'placement' : 'right',
            'title' : 'Click to request a trade for this item!'
        });  



        $('#new-acc-box').hide();
        $('#new-account-btn').click(function() {
            $('#login-box').slideUp();
            $('#new-acc-box').slideDown();

        });

        $('#relogin-btn').click(function() {
            $('#login-box').slideDown();
            $('#new-acc-box').slideUp();
        });


        $('#new-password-input-verify').on('input', function() {

            console.log("hi");
            var pass = $.trim($('#new-password-input').val());
            var pass2 = $.trim($('#new-password-input-verify').val());
            if (pass != pass2) {
                $('#verify-password-msg').empty().append("The password does not match up.");
                $('#make-new-acc-btn').addClass('disabled');
            } else {
                $('#verify-password-msg').empty();
                $('#make-new-acc-btn').removeClass('disabled');
            }
        });

		$('.box').prop('checked', true);
		$('#allbox').prop('checked', true);
		$('#allbox').change(function() {
			$('.box').prop('checked', $('#allbox').prop('checked'));
		});



        $('#new-tab').click(function() {
            $('li').removeClass('active');
            $('#new-tab').addClass('active');
            $('#items').show();
            $('#search-results').hide();
        });

        $('#search-results-tab').click(function() {
            $('li').removeClass('active');
            $('#search-results-tab').addClass('active');
            $('#items').hide();
            $('#search-results').show();
        });
        $('#search-item-input').on('input', function() {
             $('#search-results-tab').trigger('click');
             console.log("ff");
            var query = $.trim($('#search-item-input').val());
            console.log(query);
            $.ajax('/trade/csearch/' + query).done(function(resp) {
                $('#all-results').empty().append('<br>');
                console.log(resp);
                $.each(resp, function(index, entry) {
                    var username = entry.username;
                    var iid = entry.iid;
                    var picture = entry.picture;
                    var thumbnail = entry.thumbnail;
                    var description = entry.description;
                    var title = entry.title;
                    var isloggedin = entry.loggedin;
                    var requested = entry.requested;

                    var pic = $('<div class="col-md-2"/>').append('<img src="/' + thumbnail + '" width="100" height="100">');

                    var info = $('<div class="col-md-7"/>').append('<div class="iid">' + title + '</div>').append('<div class="description">' + description + '</div>');
                    
                    
                    var entry_form = $('<form action="/trade/request_item/' + iid + '/main" method="post" enctype="text/plain" />');
                    var buttons = $('<div class="item-buttons col-md-3" />');

                    if (!isloggedin) {
                        buttons.append('<input type="submit" class="btn btn-success disabled input-trade-request-nologin" name="request" value="Request Trade" />');
                    } else if (requested) {
                        buttons.append('<input type="submit" class="btn btn-success disabled input-trade-is-requested" name="request" value="Requested" />');
                    } else {
                        buttons.append('<input type="submit" class="btn btn-success input-trade-request" name="request" value="Request Trade" />');
                    }
                    buttons.append('<br> <a href="/trade/other/' + username + '"> See other items from ' + username + ' </a>')
                    entry_form.append(buttons);
                    var entry = $('<div class="well well-sm"/>').append($('<div class="row item-entry" />').append(pic).append(info).append(entry_form));
                    $('#all-results').append(entry);
                });
            });
        });
	}  
}

$(document).ready(function() {

	View.init();
                
        
})
