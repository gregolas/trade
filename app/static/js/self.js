View = {
	init : function() {
		View.init_render();
	},
	init_render : function() {
		$('#notifications').hide();


		$('.banner-header').on({
			'click' : function() {
        		window.location = "/trade/main";
        	},
        	'mouseover' : function() {
        		$(this).css('cursor', 'pointer');
        	}

        });

        $('#add-item').on({
        	'click' : function() {
        		console.log('female dogs');
        		$('#add-item-modal').modal('show');
        	},
        	'mouseover' : function() {
        		$(this).css('cursor', 'pointer');
        	}

        });

        var suggested_trade = {
        	owner : null,
        	requester : null,
        	item1 : null,
        	item2 : null
        };


        $('.suggest-trade-btn').each(function() {
        	var $btn = $(this);
        	var $terminate_btn = $btn.next();

        	var item1 = $btn.prev().attr('id');
        	var owner = $btn.prev().prev().attr('id');
        	var title = $btn.prev().prev().prev().attr('id');
        	var requester = $btn.attr('id');

        	$terminate_btn.click(function() {
        		$.post("/trade/choose_payment/terminate", {
        			owner: owner,
        			requester: requester,
        			item1: item1
        		});
        		var row_entry = $terminate_btn.parents('.notification-entry');
        		row_entry.slideUp();
        	});

        	$btn.on({
	        	'click' : function() {
	        		console.log(item1);
	        		console.log(owner);
	        		$('#all-other-items-header').empty().append("Pick an item you want for your " + title);
	        		
	        		$.ajax('/trade/get_items/' + requester).done(function(resp) {
	        			$('#all-other-items').empty();
	        			$.each(resp, function(obj_id, data) {
	        				var title = data.title;
	        				var picture_path = "/media/" + data.picture;

	        				var description = data.description;
	        				var item_entry = $('<div class="row item-entry" />');
	        				
	        				item_entry.append('<br>');
	        				var img_box = $('<div class="col-md-2"/>').append('<img src="' + picture_path + '" width="70" height="70">');
	        				var descr_box = $('<div class="description col-md-8"/>').append("<strong>" + title + "</strong><br>" + description);
	        				var btn_box = $('<div class="item-buttons col-md-2" />');
	        				var btn = $('<button type="button" class="btn btn-success choose-item-btn" />').append("Choose");
	        				btn_box.append(btn);
	        				item_entry.append(img_box).append(descr_box).append(btn_box);
	        				$('#all-other-items').append(item_entry);
	        				btn.click(function() {
	        					$('.choose-item-btn').removeClass('disabled');
	        					btn.addClass('disabled');
	        					suggested_trade.owner = owner;
	        					suggested_trade.requester = requester;
	        					suggested_trade.item1 = item1;
	        					suggested_trade.item2 = title + "_" + requester;
	        				});
	        			});
	        			$('#pick-item-modal').modal('show');
	        		});
	        	},

	        	'mouseover' : function() {
	        		$(this).css('cursor', 'pointer');
	        	}
        	});
        });

		
		$('#confirm-item-trade-btn').click(function() {
			if (suggested_trade.owner) {
				console.log(suggested_trade);

				$.post("/trade/choose_payment/suggest", suggested_trade);

				$('#pick-item-modal').modal('hide');

			} else {
				console.log("lol you didnt pick a button");
			}

		});

		$('.confirm-trade-btn').each(function() {
			var $confirm_btn = $(this);
			var $request_new_trade_btn = $confirm_btn.next();
			var $terminate_btn = $confirm_btn.next().next();
			var requester = $confirm_btn.parent().prev().attr('id');
			var item2 = $confirm_btn.parent().prev().prev().attr('id');
			var owner = $confirm_btn.parent().prev().prev().prev().attr('id');
			var item1 = $confirm_btn.parent().prev().prev().prev().prev().attr('id');
			var title1 = $confirm_btn.parent().prev().prev().prev().prev().prev().attr('id');
			var title2 = $confirm_btn.parent().prev().prev().prev().prev().prev().prev().attr('id');
			var data = {
				owner: owner,
				requester: requester,
				item1: item1,
				item2: item2
			};
//fsdfsd
			$confirm_btn.click(function() {
				console.log(data);
				var row_entry = $confirm_btn.parents('.notification-entry');
				row_entry.empty().append("&nbsp;&nbsp;&nbsp;Trade success: " + data.owner + " wants to trade his "  + title1 + " for your " + title2 + ". His email address is: " + "person@princeton.edu");
				// $.post('/trade/respond_to_payment/accept', data, function(resp) {
				// 	var owneremail = resp.owneremail;
				// 	var row_entry = $confirm_btn.parents('.notification-entry');
				// 	row_entry.empty().append(" &nbsp &nbsp  &nbsp Trade success: " + resp.owner + " wants to trade his "  + title1 + " for your " + title2 + ". <br> His email address is: " + "person@princeton.edu");
				// });
			});
			$request_new_trade_btn.click(function() {
				$.post('/trade/respond_to_payment/decline', data);
			});
			$terminate_btn.click(function() {
				$.post('/trade/respond_to_payment/terminate', data);
			});
		});

         $('#add-item').tooltip({
                'selector' : '',
                'placement' : 'right',
                'title' : 'Add another item to your inventory'
        });

         $('#items').hide().slideDown('slow');

         $('#upload-picture-header').tooltip({
                'selector' : '',
                'placement' : 'left',
                'title' : 'You have to add a picture for your item in order to add it to your inventory'
        });



         $('#add-new-item-btn').click(function() {
         	$('#add-item-modal').modal('hide');
         	data = {
         		description: $.trim($('#item-description-input').val()),
         		tags: $.trim($('#item-tags-input').val()),
         		itemname: $.trim($('#item-name-input').val())
         	};
         	console.log(document.getElementById("item-add-pic-input"));
         	$.post("/trade/add_item/", data, function(result){
    			console.log("returned that female dog");
  			});
         });   

         $('#item-name-input').on('input', function() {
             console.log('wow');
             var name = $.trim($('#item-name-input').val());
             if (name.indexOf("_") != -1) {
                 console.log('contains');
                 $('#verify-name-msg').empty().append("The name cannot contain '_' characters.");
                 $('#add-new-item-btn').addClass('disabled');
             }
             else {
                 console.log('does not contain');
                 $('#verify-name-msg').empty();
                 $('#add-new-item-btn').removeClass('disabled');
             }
         });


		$('#items-header').click(function() {
			if ($('#all-items').hasClass('open')) {
				$('#all-items').removeClass('open');
				$('#all-items').slideUp("slow", function() {
					$('#items-header').text('Your Inventory \u21e9');
				});
			} else {
				$('#all-items').addClass('open');
				$('#all-items').slideDown();
				$('#items-header').text('Your Inventory');
			}
		});

		$('#items-header').mouseover(function() {
			$(this).css('cursor', 'pointer');
		});


		$('#notifications-marker-span').on({
			'click' : function() {
        		if ($('#notifications').hasClass('open')) {
        			$('#notifications').removeClass('open').slideUp();
        			$('#notifications-indicator').empty().append('(Click here to show them)');
        		} else {
        			$('#notifications').addClass('open').slideDown();
        			$('#notifications-indicator').empty();
        		}
        	},
        	'mouseover' : function() {
        		$(this).css('cursor', 'pointer');
        	}
		});



	}
}

$(document).ready(function() {

	View.init();
                
        
})
