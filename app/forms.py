from django import forms
from django.db import models

class ImageForm(forms.Form):
    image = forms.FileField(
        label='Select a file',
    )
