from PIL import Image

DEFAULT_DIM = 100

def thumbify(name, dimension=DEFAULT_DIM):
    im = Image.open(name)
    img = im.thumbnail((dimension, dimension), Image.ANTIALIAS)
    newname = name + ".thumb.jpg"
    fout = open(newname, "w")
    im.save(newname, "jpeg", quality=100)
    fout.close()
    return newname
