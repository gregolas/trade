from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.contrib import auth
from django.template import RequestContext
from django.core.context_processors import csrf

from django.utils import simplejson

from django.contrib.auth.models import User

import logging, random, string
import thumbify, sendemail

from app.models import CustomUser, Item, PTrade, STrade, RequestedItem
from app.forms import ImageForm

IMAGE_DIR = "media/images/"

RANDOM_BANK = string.digits * 4 + string.letters

def delete(request, iid):
    if not request.user.is_authenticated():
        return HttpResponse("not logged in.")
    if not request.method == 'POST':
        return HttpResponse("not a POST.")
    item = Item.objects.get(iid=iid)
    item.delete()
    items = PTrade.objects.filter(item1=iid)
    if items:
        item.delete()
    items = PTrade.objects.filter(item2=iid)
    if items:
        item.delete()
    return HttpResponseRedirect('/trade/self')

def home(request):
    return HttpResponseRedirect('/trade/main')

def get_items(request, username):
    logging.error("JSON")
    items = Item.objects.filter(username=username)
    data = {}
    for item in items:
        itemdata = {'title': item.iid[:item.iid.index('_')],
                    'description': item.description,
                    'picture': item.picture.name}
        data[item.iid] = itemdata
    logging.error(str(data))
    data = simplejson.dumps(data)
    return HttpResponse(data, mimetype="application/json")

def password_check(username, password):
    if not password:
        return False
    if len(password) < 8:
        return False
    if username == password:
        return False
    return True

def csignup(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        email = request.POST['email']
        alphacode = request.POST['alphacode']
        if alphacode != 'princetonthisforthatalphacodebengregmichael':
            return HttpResponse('incorrect alpha code. this isn\'t public yet!')
        if '@princeton.edu' not in email:
            return HttpResponse('Princeton email required')
        if User.objects.filter(username=username):
            return HttpResponse('username exists')
        #if User.objects.filter(email=email):
            #return HttpResponse('email exists')
        if not password_check(username, password):
            return HttpResponse('bad password')
        user = User.objects.create_user(username, email, password)
        user.save()
        mrandom = ''.join(random.choice(RANDOM_BANK) for i in xrange(32))
        cuser = CustomUser(email=email, username=username, verified=False, random=mrandom)
        cuser.save()
        user = auth.authenticate(username=username, password=password)
        if user is None:
            return HttpResponse("could not authenticate.")
        if user is not None and user.is_active:
            auth.login(request, user)

        sendemail.sendemail(email, mrandom, username)
        return HttpResponseRedirect('/trade/main')
    return HttpResponse('not a post')

def self(request):
    if not request.user.is_authenticated():
        return HttpResponse('not logged in.')
    items = Item.objects.filter(username=request.user.get_username())
    ptrades = PTrade.objects.filter(owner=request.user.get_username())#.order_by('-date')
    otherptrades = PTrade.objects.filter(requester=request.user.get_username())#.order_by('-date')
    strades1 = STrade.objects.filter(owner=request.user.get_username())#.order_by('-date')
    strades2 = STrade.objects.filter(requester=request.user.get_username())#.order_by('-date')
    from itertools import chain
    strades = list(chain(strades1, strades2))
    notifications = False
    num_notifications = 0
    finalptrades=[]
    for item in items:
        item.title = item.iid[:item.iid.index('_')]
    if ptrades:
        for ptrade in ptrades:
            ptrade.title = ptrade.item1[:ptrade.item1.index('_')]
            if not ptrade.item2:
                notifications = True
                num_notifications += 1
                finalptrades.append(ptrade)
    responses = []
    if otherptrades:
        for ptrade in otherptrades:
            if ptrade.item2:
                notifications = True
                num_notifications += 1
                ptrade.title1 = ptrade.item1[:ptrade.item1.index('_')]
                ptrade.title2 = ptrade.item2[:ptrade.item2.index('_')]
                responses.append(ptrade)
    for strade in strades:
        notifications = True
        o = strade.owner
        r = strade.requester
        owneruser = CustomUser.objects.get(username=o)
        owneremail = owneruser.email
        requesteruser = CustomUser.objects.get(username=r)
        requesteremail = requesteruser.email
        strade.owneremail = owneremail
        strade.requesteremail = requesteremail
        strade.title1 = strade.item1[:strade.item1.index('_')]
        strade.title2 = strade.item2[:strade.item2.index('_')]
        num_notifications += 1
    logging.error("ben strades: " + str(strades))
    return render(request, 'self.html', {'strades': strades, 'responses': responses, 'num_notifications': num_notifications, 'notifications': notifications, 'items': items, 'username': request.user.get_username(), 'ptrades': finalptrades}, context_instance=RequestContext(request))

def other(request, username):
    muser = None
    if request.user.is_authenticated():
        muser = request.user.get_username()
        if muser == username:
            return HttpResponseRedirect("/trade/self")
    if not CustomUser.objects.filter(username=username):
        return HttpResponse('invalid username.')
    otheruser = username
    items = Item.objects.filter(username=otheruser)
    for item in items:
        item.title = item.iid[:item.iid.index('_')]
        if PTrade.objects.filter(requester=muser, item1=item.iid):
            item.requested = True
        else:
            item.requested = False
    return render(request, 'other.html', {'otheruser': otheruser, 'username': muser, 'items': items}, context_instance=RequestContext(request))

def request_item(request, iid, loc):
    if not request.user.is_authenticated():
        return HttpResponse('not logged in.')
    if request.method != 'POST':
        logging.error(request.method)
        return HttpResponse('not POST.')
    else:
        logging.error('POSTTTTTTT!!!!!!!')
    item = Item.objects.filter(iid=iid)
    if not item:
        return HttpResponse('item doesn\'t exist anymore')
    req = RequestedItem(username=request.user.get_username(), iid=iid)
    req.save()
    ptrade = PTrade(owner=iid[iid.index('_')+1:], requester=request.user.get_username(), item1=iid, seen=False)
    ptrade.save()
    if loc == 'other':
        return HttpResponseRedirect('/trade/other/' + iid[iid.index('_')+1:])
    else:
        return HttpResponseRedirect('/trade/main')

def csearch(request, query):
    results = Item.objects.filter(description__contains=query)
    results = list(results)
    tags = Item.objects.filter(tags__contains=query)
    tags = list(tags)
    iid = Item.objects.filter(iid__contains=query)
    iid = list(iid)
    results = set(results + tags + iid)
    ret = list()
    user = None
    loggedin = False
    if request.user.is_authenticated():
        user = request.user.get_username()
        loggedin = True
    for result in results:
        result.title = result.iid[:result.iid.index('_')]
        if result.username == user:
            continue
        ptrade = PTrade.objects.filter(requester=user, item1=result.iid)
        requested = False
        if len(ptrade) >= 1:
            requested = True
        data = {'username': result.username,
                'iid': result.iid,
                'picture': result.picture.name,
                'thumbnail': result.thumbnail,
                'description': result.description,
                'loggedin': loggedin,
                'requested': requested,
                'title': result.title}
        ret.append(data)
    results = list(ret)
    data = simplejson.dumps(results)
    return HttpResponse(data, mimetype="application/json")

def clogin(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    logging.error("username, password: " + username+ ", " + password)
    user = auth.authenticate(username=username, password=password)
    if user is None:
        return HttpResponse("could not authenticate.")
    if user is not None and user.is_active:
        auth.login(request, user)
        return HttpResponseRedirect("../main")
    else:
        return HttpResponse("failed.")

def main(request):
    items = Item.objects.all()
    if request.user.is_authenticated():
        user=request.user.get_username()
    else:
        user=None
    for item in items:
        item.title = item.iid[:item.iid.index('_')]
        if item.username == user:
            item.own = True
            continue
        else:
            item.own = False
        if PTrade.objects.filter(requester=user, item1=item.iid):
            item.requested = True
        else:
            item.requested = False
    return render(request, 'main.html', {'username': user, 'items': items}, context_instance=RequestContext(request))

def verify_email(request, random_username):
    mrandom=random_username[:32]
    username=random_username[32:]
    logging.error("OMGGG" + username)
    cuser = CustomUser.objects.get(username=username)
    if cuser.random == mrandom:
        cuser.verified=True
        cuser.save()
        return HttpResponse('verified.')
    else:
        return HttpResponse('not verified.')

def trade(request):
    # go delete all necessary requests
    return HttpResponse('trade')

def choose_payment(request, action):
    if not request.user.is_authenticated():
        return HttpResponse("not logged in.")
    if not request.method == 'POST':
        return HttpResponse("not a POST.")
    owner = request.POST['owner']
    requester = request.POST['requester']
    item1 = request.POST['item1']
    ptrade = PTrade.objects.get(owner=request.user.get_username(), requester=requester, item1=item1)
    logging.error("ACTION: " + action)
    if action == 'terminate':
        ptrade.delete()
    else:
        item2 = request.POST['item2']
        ptrade.item2 = item2
        ptrade.save()
    return HttpResponse('choose')

def obliterate(item1, item2):
    items = Item.objects.filter(iid=item1)
    items.delete()
    items = Item.objects.filter(iid=item2)
    items.delete()
    items = PTrade.objects.filter(item1=item1)
    items.delete()
    items = PTrade.objects.filter(item2=item1)
    items.delete()
    items = PTrade.objects.filter(item1=item2)
    items.delete()
    items = PTrade.objects.filter(item2=item2)
    items.delete()

def respond_to_payment(request, action):
    if not request.user.is_authenticated():
        return HttpResponse("not logged in.")
    if not request.method == 'POST':
        return HttpResponse("not a POST.")
    logging.error("POST: " + str(request.POST))
    owner = request.POST['owner']
    requester = request.POST['requester']
    item1 = request.POST['item1']
    user = request.user.get_username()
    if action == 'accept':
        item2 = request.POST['item2']
        ptrade = PTrade.objects.get(owner=owner, requester=requester, item1=item1, item2=item2)
        strade = STrade(owner=owner, requester=requester, item1=item1, item2=item2)
        strade.save()
        ptrade.delete()
        owneruser = CustomUser.objects.get(username=owner)
        owneremail = owneruser.email
        requesteruser = CustomUser.objects.get(username=requester)
        requesteremail = requesteruser.email
        item1name = item1[:item1.index('_')]
        item2name = item2[:item2.index('_')]
        data = {'title2': item2name, 'title1': item1name, 'owner': owner, 'requester': requester, 'item1': item1, 'item2': item2, 'owneremail': owneremail, 'requesteremail': requesteremail}
        data = simplejson.dumps(data)
        obliterate(item1, item2)
        return HttpResponse(data, mimetype="application/json")
    elif action == 'terminate':
        ptrade = PTrade.objects.get(owner=owner, requester=requester, item1=item1)
        ptrade.delete()
    else:   # action == 'decline'
        ptrade = PTrade.objects.get(owner=owner, requester=requester, item1=item1)
        ptrade.item2 = ''
        ptrade.save()
    return HttpResponse('respond')

def add_item(request):
    if not request.user.is_authenticated():
        return HttpResponse('not logged in.')
    if request.method == 'POST':
        username = request.user.get_username()
        form = ImageForm(request.POST, request.FILES)
        tags = request.POST['tags']
        itemname = request.POST['itemname']
        itemname += "_" + username
        image = request.FILES['image']
        logging.error('type image: ' + str(type(image)))
        description = request.POST['description']
        newitem = Item(username=username, picture=image, active=False, description=description, numclicks=0, tags=tags, iid=itemname)
        newitem.save()
        thumbnail = thumbify.thumbify(IMAGE_DIR + username + "/" + image.name)
        newitem.thumbnail=thumbnail
        newitem.save()
        return HttpResponseRedirect("/trade/self")
    else:
        form = ImageForm()
    return HttpResponse('not a post.')

def clogout(request):
    auth.logout(request)
    return HttpResponseRedirect("../main")
