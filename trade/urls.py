from django.conf.urls import patterns, include, url
from app.views import home, main, self, other, add_item, clogin, clogout, request_item, csignup, verify_email, get_items, respond_to_payment, choose_payment, delete, csearch

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'trade.views.home', name='home'),
    # url(r'^trade/', include('trade.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^home/$', home),
    url(r'^main/$', main),
    url(r'^self/$', self),
    url(r'^other/(.*)', other),
    url(r'^request_item/(.*)/(.*)', request_item),
    url(r'^add_item/$', add_item),
    url(r'^clogin/$', clogin),
    url(r'^clogout/$', clogout),
    url(r'^csignup/$', csignup),
    url(r'^csearch/(.*)', csearch),
    url(r'^delete/(.*)', delete),
    url(r'^choose_payment/(.*)', choose_payment),
    url(r'^respond_to_payment/(.*)', respond_to_payment),
    url(r'^verify_email/(.*)', verify_email),
    url(r'^get_items/(.*)', get_items),
    url(r'^$' , home),
)
